#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "Log.h"
#include "Model.h"
#include <vector>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27016"

int __cdecl main(int argc, char **argv) 
{
    // Validate the parameters
    if (argc < 2) {
        log("usage: ConsoleClient server-name\n");
        return 1;
    }

	Triangles triangles;
    if (argc >= 3) {
  		for (int i = 2; i < argc; i++) {
			//triangles.push_back(i);
			triangles.push_back(atoi(argv[i]));
		}
    }
	else {
		// Generate random list of triangles
		for (int i = 0; i < 10; i++) {
			//triangles.push_back(i);
			triangles.push_back(rand() % 1760);
		}
		log("generate %i trinagles\n", (int)triangles.size());
		for (Triangles::size_type i = 0; i < triangles.size(); i++) {
			log("%i ", triangles[i]);
		}
		log("\n");
	}

	WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                    *ptr = NULL,
                    hints;
    int iResult;
    
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        log("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {
        log("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, 
            ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            log("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        log("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }

	// �������� ����� - ���������� �������������
	int size = triangles.size();
	iResult = SendToSocket(ConnectSocket, (char *)(&size), sizeof(size));
    log("Bytes Sent: %ld\n", iResult);

	// �������� ������ �������������
	int sendBufferSize = (int)(triangles.size() * sizeof(triangles[0]));
	char *buffer = (char*)(&(triangles[0]));
	iResult = SendToSocket(ConnectSocket, buffer, sendBufferSize); 
    log("Bytes Sent: %ld\n", iResult);

    // shutdown the connection since no more data will be sent
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        log("shutdown failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

	Vertices vert;
	{
		// ������� ����� - ���������� ������
		int vertCount;
		if (!ReceiveFromSocket(ConnectSocket, (char *)(&vertCount), sizeof(vertCount))) {
			return 3;
		}
		vert.resize(vertCount);
		// ������� ������ ������
		if (!ReceiveFromSocket(ConnectSocket, (char *)(&vert[0]), (int)(vert.size() * sizeof(vert[0])))) {
			return 3;
		}
	}
	Indexes ind;
	{
		// ������� ����� - ���������� ��������
		int indCount;
		if (!ReceiveFromSocket(ConnectSocket, (char *)(&indCount), sizeof(indCount))) {
			return 3;
		}
		if (indCount % 3 != 0)
		{
			log("Wrong data received: %i\n", indCount);
			return 3;
		}
		ind.resize(indCount);
		// ������� ������ ������
		if (!ReceiveFromSocket(ConnectSocket, (char *)(&ind[0]), (int)(ind.size() * sizeof(ind[0])))) {
			return 3;
		}
	}

	Model model(vert, ind);
	model.Print();
    // cleanup
    closesocket(ConnectSocket);
    WSACleanup();

    return 0;
}
