#include "Model.h"
#include "Log.h"

bool Model::LoadObj(char *filename)
{
	FILE * file = fopen(filename, "rt");
	if( file == NULL ){
		log("Impossible to open the file: %s!\n", filename);
		return false;
	}
	_vert.clear();
	_indexes.clear();
	char lineHeader[512];
	while (true) {
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF) {
			break; // EOF = End Of File. Quit the loop.
		}
		else if ( strcmp( lineHeader, "v" ) == 0 ){
			Vertex v;
			fscanf(file, "%f %f %f\n", &v.x, &v.y, &v.z);
			_vert.push_back(v);
		}			
		else if ( strcmp( lineHeader, "f" ) == 0 ){
			Indexes v(3);
			fscanf(file, "%i %i %i\n", &v[0], &v[1], &v[2]);
			_indexes.insert(_indexes.end(), v.begin(), v.end());
		}			
	}
	fclose(file);
	return true;
}

int Model::GetIndex(int indexOrigin, std::map<int, int> &vertMap, Vertices &vert) const
{
	indexOrigin--;
	if (vertMap.find(indexOrigin) == vertMap.end()) {
		vert.push_back(_vert[indexOrigin]);
		vertMap[indexOrigin] = vert.size();
	}
	return vertMap[indexOrigin];
}

void Model::GetSubModel(const Triangles &tri, Vertices &vert, Indexes &ind) const
{
	vert.clear();
	ind.clear();
	std::map<int, int> vertMap;
	for (Triangles::size_type i = 0; i < tri.size(); i++) 
	{
		if (tri[i] * 3 + 2 >= (int)_indexes.size()) {
			log("Wrong triangle list!\n");
			return;
		}
		ind.push_back(GetIndex(_indexes[tri[i] * 3 + 0], vertMap, vert));
		ind.push_back(GetIndex(_indexes[tri[i] * 3 + 1], vertMap, vert));
		ind.push_back(GetIndex(_indexes[tri[i] * 3 + 2], vertMap, vert));
	}
}

Model::Model(const Vertices &vert, const Indexes &ind)
{
	_vert = vert;
	_indexes = ind;
}

void Model::Print()
{
	log("# Model\n");
	log("# %i points\n", (int)(_vert.size()));
	log("# %i vertices\n", (int)(_indexes.size()));
	for (Vertices::size_type i = 0; i < _vert.size(); i++)
	{
		log("v %f %f %f\n", _vert[i].x, _vert[i].y, _vert[i].z);
	}
	for (Indexes::size_type i = 0; i < _indexes.size(); i += 3)
	{
		log("f %i %i %i\n", _indexes[i], _indexes[i + 1], _indexes[i + 2]);
	}
	log("# %i points\n", (int)(_vert.size()));
	log("# %i vertices\n", (int)(_indexes.size()));
}