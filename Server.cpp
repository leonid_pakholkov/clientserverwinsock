#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <process.h>
#include "log.h"
#include "Model.h"

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27016"

Model model;
HANDLE hMutex;

unsigned __stdcall ClientSession(void *data)
{
	int iResult;
    SOCKET client_socket = (SOCKET)data;
 
	// Process the client.
 
	// ������� ����� - ���������� ������������
	int trianglesCount;
	if (!ReceiveFromSocket(client_socket, (char *)(&trianglesCount), sizeof(trianglesCount))) {
		return 3;
	}
	// ������� ������ ������������
	Triangles triangles(trianglesCount);
	if (!ReceiveFromSocket(client_socket, (char *)(&triangles[0]), (int)(triangles.size() * sizeof(triangles[0])))) {
		return 3;
	}
	log("%i trinagles\n", (int)triangles.size());
	for (Triangles::size_type i = 0; i < triangles.size(); i++) {
		log("%i ", triangles[i]);
	}
	log("\n");

	Vertices vert;
	Indexes ind;
	WaitForSingleObject(hMutex, INFINITE);
	model.GetSubModel(triangles, vert, ind); 
	ReleaseMutex(hMutex);

	{
		// �������� ����� - ���������� ������
		int size = vert.size();
		iResult = SendToSocket(client_socket, (char *)(&size), sizeof(size));
		log("Bytes Sent: %ld, verticies %i\n", iResult, size);

		// �������� ������ ������
		int sendBufferSize = (int)(vert.size() * sizeof(vert[0]));
		char *buffer = (char*)(&(vert[0]));
		iResult = SendToSocket(client_socket, buffer, sendBufferSize); 
		log("Bytes Sent: %ld\n", iResult);
	}

	{
		// �������� ����� - ���������� ��������
		int size = ind.size();
		iResult = SendToSocket(client_socket, (char *)(&size), sizeof(size));
		log("Bytes Sent: %ld, indexes %i\n", iResult, size);

		// �������� ������ ��������
		int sendBufferSize = (int)(ind.size() * sizeof(ind[0]));
		char *buffer = (char*)(&(ind[0]));
		iResult = SendToSocket(client_socket, buffer, sendBufferSize); 
		log("Bytes Sent: %ld\n", iResult);
	}


//	iResult = SendToSocket(client_socket,  (char *)(&triangles[0]), (int)(triangles.size() * sizeof(triangles[0])));

    // shutdown the connection since we're done
    iResult = shutdown(client_socket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        log("shutdown failed with error: %d\n", WSAGetLastError());
        closesocket(client_socket);
        //WSACleanup();
        return 3;
    }
	return 0;
 }

int __cdecl main(int argc, char **argv) 
{
	if (argc != 2) {
		log("usage: ConsoleServer obj_file\n");
		return 1;
	}
	WSADATA wsaData;
    int iResult;
	log("Loading: %s\n", argv[1]);
	if (!model.LoadObj(argv[1])) {
		return 10;
	}

    SOCKET ListenSocket = INVALID_SOCKET;
    SOCKET ClientSocket = INVALID_SOCKET;

    struct addrinfo *result = NULL;
    struct addrinfo hints;
    
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        log("WSAStartup failed with error: %d\n", iResult);
        return 4;
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    // Resolve the server address and port
    iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {
        log("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 5;
    }

    // Create a SOCKET for connecting to server
    ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (ListenSocket == INVALID_SOCKET) {
        log("socket failed with error: %ld\n", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return 6;
    }
	u_long iMode=1;
	ioctlsocket(ListenSocket,FIONBIO,&iMode);

    // Setup the TCP listening socket
    iResult = bind( ListenSocket, result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        log("bind failed with error: %d\n", WSAGetLastError());
        freeaddrinfo(result);
        closesocket(ListenSocket);
        WSACleanup();
        return 7;
    }

    freeaddrinfo(result);

    iResult = listen(ListenSocket, SOMAXCONN);
    if (iResult == SOCKET_ERROR) {
        log("listen failed with error: %d\n", WSAGetLastError());
        closesocket(ListenSocket);
        WSACleanup();
        return 8;
    }

	hMutex = CreateMutex(NULL, FALSE, NULL);

	printf("Start listening. Use key 'Esc' to exit.\n");
	// Accept a client sockets
	while ((ClientSocket = accept(ListenSocket, NULL, NULL))) {
		if (ClientSocket == INVALID_SOCKET) {
			int error = WSAGetLastError();
			if (error != WSAEWOULDBLOCK) {
				log("accept failed with error: %d\n", error);
				closesocket(ListenSocket);
				WSACleanup();
				return 9;
			}
			else {
				SHORT keyesc = 0;
				keyesc = GetKeyState(VK_ESCAPE);
				if ((keyesc&0x8000)) {
					//ESC Key pressed
					break;
				}
			}
 		}
		else {
			log("Create a new thread for the accepted client\n");
			// Create a new thread for the accepted client (also pass the accepted client socket).
			unsigned threadID;
			HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &ClientSession, (void*)ClientSocket, 0, &threadID);
		}
    }

	// cleanup
    closesocket(ClientSocket);
    WSACleanup();
    
	// No longer need server socket
    closesocket(ListenSocket);

	log("Exit.\n");

    return 0;
}

