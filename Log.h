#ifndef LOG_H
#define LOG_H

#include <winsock2.h>

void log(const char * format, ...);
int SendToSocket(SOCKET socket, const char *buffer, int size);
bool ReceiveFromSocket(SOCKET socket, char *buffer, int size);

#endif//LOG_H