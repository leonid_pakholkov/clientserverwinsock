#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <map>

struct Vertex
{
	float x;
	float y;
	float z;
};

typedef std::vector<int> Indexes;
typedef Indexes Triangles;
typedef std::vector<Vertex> Vertices;

class Model
{
private:
	Vertices _vert;
	Indexes _indexes;
	int GetIndex(int indexOrigin, std::map<int, int> &vertMap, Vertices &vert) const;
public:
	Model() {}
	Model(const Vertices &vert, const Indexes &ind);
	bool LoadObj(char *filename);
	void GetSubModel(const Triangles &tri, Vertices &vert, Indexes &ind) const;
	void Print();
};

#endif//MODEL_H