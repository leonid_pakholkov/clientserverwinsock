#include "log.h"
#include <stdio.h>
#include <windows.h>

void log(const char * format, ...)
{
	char buff[1000];
	va_list args;
	va_start(args, format);
	vsnprintf_s(buff, 1000, format, args);
	va_end(args);

	printf(buff);
#ifdef _DEBUG
	OutputDebugStringA(buff);
#endif
}

int SendToSocket(SOCKET socket, const char *buffer, int size)
{
	int result = send( socket, buffer, size, 0 );
	if (result == SOCKET_ERROR) {
		log("send failed with error: %d\n", WSAGetLastError());
		closesocket(socket);
		WSACleanup();
		exit(2);
	}
	return result;
}

bool ReceiveFromSocket(SOCKET socket, char *buffer, int size)
{
   // Receive until the peer shuts down the connection
	int iResult;
	do {
        iResult = recv(socket, buffer, size, 0);
        if (iResult > 0) {
            log("Bytes received: %d\n", iResult);
			if (iResult < size) {
				buffer += iResult;
				size -= iResult;
				iResult = 0;
			}
        }
        else if (iResult == 0) {
            log("Connection closing...\n");
			return false;
		}
        else  {
            log("recv failed with error: %d\n", WSAGetLastError());
            closesocket(socket);
            //WSACleanup();
            return false;
        }

    } while (iResult != size);
	return true;
}

